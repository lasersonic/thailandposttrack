//
//  TrackActivity.h
//  thaiposttrack
//
//  Created by Nutthawut on 5/5/13.
//  Copyright (c) 2013 Nut Tang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TrackActivity : NSObject
@property (strong, nonatomic) NSString *datetime, *department, *description, *result;
@end
