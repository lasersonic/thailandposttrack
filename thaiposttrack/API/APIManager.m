//
//  APIManager.m
//  thaiposttrack
//
//  Created by Nutthawut on 5/5/13.
//  Copyright (c) 2013 Nut Tang. All rights reserved.
//

#import "APIManager.h"
#import "SBJson.h"
#import "TrackActivity.h"

@implementation APIManager


-(MKNetworkOperation*) getActivitiesFromTrackID:(NSString*) track
                                     onComplete:(Success) successBlock
                                        onError:(onError) errorBlock{
    
    NSDictionary *param = @{@"track":track};
    
    MKNetworkOperation *op = [self operationWithPath:@"emstrack.php"
                                                                    params:param
                                                                httpMethod:@"GET"];

    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        SBJsonParser *parser = [[SBJsonParser alloc] init];
        NSDictionary *root = [parser  objectWithString:[completedOperation responseString]];
        NSMutableArray *activity = [NSMutableArray array];
        int result_code = [[root objectForKey:@"result_code"] integerValue];
        
        if(result_code != 1){
            successBlock(activity);
        }else{
            NSArray *arrayDetail = [root objectForKey:@"detail"];
            
            for(int i=0;i<[arrayDetail count];i++){
                NSDictionary *act = [arrayDetail objectAtIndex:i];
                TrackActivity *track = [[TrackActivity alloc]init];
       
                if([[act objectForKey:@"datetime"] isEqual:[NSNull null]]){
                    track.datetime = @"-";
                }else{
                    track.datetime = [act objectForKey:@"datetime"];
                }
                
                if([[act objectForKey:@"department"] isEqual:[NSNull null]]){
                    track.department = @"-";
                }else{
                    track.department = [act objectForKey:@"department"];
                }
                
                if([[act objectForKey:@"description"] isEqual:[NSNull null]]){
                    track.description = @"-";
                }else{
                    track.description = [act objectForKey:@"description"];
                }
                
                if([[act objectForKey:@"result"] isEqual:[NSNull null]]){
                    track.result = @"-";
                }else{
                    track.result = [act objectForKey:@"result"];
                }
                                
                [activity addObject:track];
            }
            
            successBlock(activity);
        }
        
        //;
    } errorHandler:^(MKNetworkOperation *completedOperation, NSError *error) {
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
    
    return op;
}

@end
