//
//  APIManager.h
//  thaiposttrack
//
//  Created by Nutthawut on 5/5/13.
//  Copyright (c) 2013 Nut Tang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface APIManager : MKNetworkEngine

typedef void (^Success)(NSArray *activityArray);
typedef void (^onError)(NSError *error);

-(MKNetworkOperation*) getActivitiesFromTrackID:(NSString*) track
                                     onComplete:(Success) successBlock
                                        onError:(onError) errorBlock;
@end
