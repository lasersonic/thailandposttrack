//
//  AppDelegate.h
//  thaiposttrack
//
//  Created by Nutthawut on 5/4/13.
//  Copyright (c) 2013 Nut Tang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APIManager.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

#define ApplicationDelegate ((AppDelegate *)[UIApplication sharedApplication].delegate)


@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) APIManager *apiManager;

@end
