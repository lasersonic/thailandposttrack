//
//  ResultViewController.m
//  thaiposttrack
//
//  Created by Nutthawut on 5/4/13.
//  Copyright (c) 2013 Nut Tang. All rights reserved.
//

#import "ResultViewController.h"
#import "AppDelegate.h"
#import "TrackActivity.h"
#import "ResultCell.h"
#import "GADBannerView.h"

@interface ResultViewController (){
    NSArray *trackArray;
}

@end

@implementation ResultViewController

@synthesize emsID;

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
//    UIBarButtonItem *customItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
//    
//    self.navigationItem.backBarButtonItem = customItem;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setTitle:[emsID uppercaseString]];
    NSLog(@"EMSID %@", emsID);
    
    trackArray = [NSMutableArray array];
    
    [ApplicationDelegate.apiManager getActivitiesFromTrackID:emsID onComplete:^(NSArray *activityArray) {
        trackArray = activityArray;
        NSLog(@"data count %d", [trackArray count]);
        [self.pageControl setNumberOfPages:[trackArray count]];
        
        if([trackArray count]==0){
            [self.lblNotFound setAlpha:1];
        }
        
        for(int i=0;i<[trackArray count];i++){
            
            TrackActivity *track = [trackArray objectAtIndex:i];
            
            UILabel *lblDate = [[UILabel alloc]init];
            [lblDate setFrame:CGRectMake(320*i, 0, 320, 30)];
            [lblDate setText:@"วันเวลา"];
            [lblDate setTextColor:[UIColor whiteColor]];
            [lblDate setBackgroundColor:[UIColor colorWithRed:0.9 green:0.0 blue:0.0 alpha:1]];
            
            UILabel *lblDepartment = [[UILabel alloc]init];
            [lblDepartment setFrame:CGRectMake(320*i, 74, 320, 30)];
            [lblDepartment setText:@"สถานที่"];
            [lblDepartment setTextColor:[UIColor whiteColor]];
            [lblDepartment setBackgroundColor:[UIColor colorWithRed:0.9 green:0.0 blue:0.0 alpha:1]];
            
            UILabel *lblDescription = [[UILabel alloc]init];
            [lblDescription setFrame:CGRectMake(320*i, 148, 320, 30)];
            [lblDescription setText:@"คำอธิบาย"];
            [lblDescription setTextColor:[UIColor whiteColor]];
            [lblDescription setBackgroundColor:[UIColor colorWithRed:0.9 green:0.0 blue:0.0 alpha:1]];
            
            UILabel *lblResult = [[UILabel alloc]init];
            [lblResult setFrame:CGRectMake(320*i, 222, 320, 30)];
            [lblResult setText:@"ผลการนำส่ง"];
            [lblResult setTextColor:[UIColor whiteColor]];
            [lblResult setBackgroundColor:[UIColor colorWithRed:0.9 green:0.0 blue:0.0 alpha:1]];
            
            UILabel *lblDateValue = [[UILabel alloc]init];
            [lblDateValue setFrame:CGRectMake(320*i, 30, 300, 44)];
            [lblDateValue setText: track.datetime];
            [lblDateValue setTextColor:[UIColor darkGrayColor]];
            [lblDateValue setTextAlignment:NSTextAlignmentRight];
            
            UILabel *lblDepartmentValue = [[UILabel alloc]init];
            [lblDepartmentValue setFrame:CGRectMake(320*i, 74+30, 300, 44)];
            [lblDepartmentValue setText: track.department];
            [lblDepartmentValue setTextColor:[UIColor darkGrayColor]];
            [lblDepartmentValue setTextAlignment:NSTextAlignmentRight];
            
            UILabel *lblDescValue = [[UILabel alloc]init];
            [lblDescValue setFrame:CGRectMake(320*i, 148+30, 300, 44)];
            [lblDescValue setText: track.description];
            [lblDescValue setTextColor:[UIColor darkGrayColor]];
            [lblDescValue setTextAlignment:NSTextAlignmentRight];
            
            UILabel *lblResultValue = [[UILabel alloc]init];
            [lblResultValue setFrame:CGRectMake(320*i, 222+30, 300, 44)];
            NSLog(@"%@", track.result);
            if (track.result.length == 1) {
                track.result = @"-";
            }
            [lblResultValue setText: track.result];
            [lblResultValue setTextColor:[UIColor darkGrayColor]];
            [lblResultValue setTextAlignment:NSTextAlignmentRight];
            
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 296)];
            
            [view addSubview:lblDate];
            [view addSubview:lblDepartment];
            [view addSubview:lblDescription];
            [view addSubview:lblResult];
            [view addSubview:lblDateValue];
            [view addSubview:lblDepartmentValue];
            [view addSubview:lblDescValue];
            [view addSubview:lblResultValue];
            
            [self.scrollView addSubview:view];
        }
        
        [self.scrollView setContentSize:CGSizeMake(320*[trackArray count], self.scrollView.frame.size.height)];
        
    } onError:^(NSError *error) {
        [self.scrollView setAlpha:0];
    }];
    
    //add Admob
    GADBannerView *bannerView_ = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner];
    bannerView_.adUnitID = @"a151852abb5713f";
    bannerView_.rootViewController = self;
    GADRequest *request = [GADRequest request];
    //request.testDevices = [NSArray arrayWithObjects:@"GAD_SIMULATOR_ID", nil];
    UIView *admobView = (UIView*)[self.view viewWithTag:990];
    [admobView addSubview:bannerView_];
    [bannerView_ loadRequest:request];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setScrollView:nil];
    [self setPageControl:nil];
    [self setLblNotFound:nil];
    [self setBarbuttonBookmark:nil];
    [super viewDidUnload];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    int page = round(scrollView.contentOffset.x/scrollView.frame.size.width);
    [self.pageControl setCurrentPage:page];
}

- (void) showMenu{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:@"ยกเลิก" destructiveButtonTitle:@"เพิ่มลงรายการโปรด" otherButtonTitles:nil];
    
    [actionSheet showInView:self.view];
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSLog(@"%d", buttonIndex);
    
    if(buttonIndex == 0){
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"ชื่อพัสดุ" message:@"" delegate:self cancelButtonTitle:@"ยกเลิก" otherButtonTitles:@"เพิ่ม", nil];
        alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
        [alertView show];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    //add to favorite
    if(buttonIndex == 1){
        NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
        NSMutableArray *trackList = [pref objectForKey:@"tracklist"];
        NSMutableArray *temp;
        if(trackList == nil){
            temp = [NSMutableArray array];
        }else{
            temp = [trackList mutableCopy];
        }
        
        NSDictionary *tracklistValue = @{@"name":[[alertView textFieldAtIndex:0] text]
                                         , @"ems":emsID
                                         , @"created":[NSDate date]};
        [temp addObject:tracklistValue];
        
        [pref setObject:temp forKey:@"tracklist"];
        //NSLog(@"add %@", trackList);
        
        [pref synchronize];
        
    }
}

- (IBAction)actionBookmark:(id)sender {
    [self showMenu];
}

@end
