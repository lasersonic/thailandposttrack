//
//  HomeViewController.h
//  thaiposttrack
//
//  Created by Nutthawut on 5/4/13.
//  Copyright (c) 2013 Nut Tang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootViewController.h"

@interface HomeViewController : RootViewController<UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate, UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UIButton *btnFind;
@property (strong, nonatomic) IBOutlet UITextField *textFieldNumber;
- (IBAction)btnFindAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnTrack;
@property (strong, nonatomic) IBOutlet UIButton *btnSearch;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UITableView *tableViewFavorite;
- (IBAction)btnShowTrackListAction:(id)sender;
- (IBAction)btnShowSearchAction:(id)sender;

@end
