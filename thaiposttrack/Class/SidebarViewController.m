//
//  SidebarViewController.m
//  thaiposttrack
//
//  Created by Nutthawut on 6/1/13.
//  Copyright (c) 2013 Nut Tang. All rights reserved.
//

#import "SidebarViewController.h"

@interface SidebarViewController (){
    NSMutableArray *menuArray;
}

@end

@implementation SidebarViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.slidingViewController setAnchorRightRevealAmount:280.0f];
    self.slidingViewController.underLeftWidthLayout = ECFullWidth;
    menuArray = [NSMutableArray array];
    [menuArray addObjectsFromArray:@[@"ติดตามพัสดุ",@"รายการโปรด"]];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [menuArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"SideBarCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier ];
    
    UILabel *lblMenu = (UILabel*)[cell viewWithTag:1];
    UIImageView *imgBadge = (UIImageView*)[cell viewWithTag:2];
    
    [lblMenu setText:[menuArray objectAtIndex:indexPath.row]];
    if(indexPath.row==0){
        [imgBadge setImage:[UIImage imageNamed:@"search"]];
    }
    if(indexPath.row==1){
        [imgBadge setImage:[UIImage imageNamed:@"fav"]];
    }
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UINavigationController *newTopViewController;
    
    if (indexPath.row == 0) {
        newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"navHome"];
    }
    
    if (indexPath.row == 1) {
        newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"navFavorite"];
    }
    
    if (indexPath.row == 2) {
        newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"navSetting"];
    }
    
    if (indexPath.row == 3) {
        newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"navStore"];
    }
    
    [self.slidingViewController anchorTopViewOffScreenTo:ECRight animations:nil onComplete:^{
        CGRect frame = self.slidingViewController.topViewController.view.frame;
        self.slidingViewController.topViewController = newTopViewController;
        self.slidingViewController.topViewController.view.frame = frame;
        [self.slidingViewController resetTopView];
    }];
}

@end
