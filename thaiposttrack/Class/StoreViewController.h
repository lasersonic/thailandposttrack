//
//  StoreViewController.h
//  thaiposttrack
//
//  Created by Nutthawut on 6/1/13.
//  Copyright (c) 2013 Nut Tang. All rights reserved.
//

#import "RootViewController.h"

@interface StoreViewController : RootViewController
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@end
