//
//  HomeViewController.m
//  thaiposttrack
//
//  Created by Nutthawut on 5/4/13.
//  Copyright (c) 2013 Nut Tang. All rights reserved.
//

#import "HomeViewController.h"
#import "ResultViewController.h"
#import "GADBannerView.h"

#import "TrackListCell.h"

@interface HomeViewController (){
    ResultViewController *resultView;
    GADBannerView *bannerView_;
    UIBarButtonItem *editButton, *donebutton;
    NSMutableArray *dataTableArray;
    NSUserDefaults *pref;
}

@end

@implementation HomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

        editButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editTable)];
        donebutton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(editTable)];
        dataTableArray = [NSMutableArray array];
        pref = [NSUserDefaults standardUserDefaults];
        self.title = @"ติดตามพัสดุ";
        
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    
    NSMutableArray *a = [pref objectForKey:@"tracklist"];
    if(a){
        dataTableArray = [a mutableCopy];
    }
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Store" style:UIBarButtonItemStylePlain target:self action:nil];
    
    [self.tableViewFavorite reloadData];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"HomeViewController" owner:self options:nil];
    
    UIView *inputView = [arrayOfViews objectAtIndex:1];
    UIView *favView = [arrayOfViews objectAtIndex:2];
    
    [inputView setFrame:CGRectMake(0, 0, 320, inputView.frame.size.height)];
    [favView setFrame:CGRectMake(320, 0, 320, favView.frame.size.height)];
    
    [self.scrollView addSubview:inputView];
    [self.scrollView addSubview:favView];
    
    //[self.scrollView setFrame:CGRectMake(0, 0, 320, self.scrollView.frame.size.height)];
    [self.scrollView setContentSize:CGSizeMake(640,0)];
    
    //add Admob
    bannerView_ = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner];
    bannerView_.adUnitID = @"a151852abb5713f";
    bannerView_.rootViewController = self;
    GADRequest *request = [GADRequest request];
//    request.testDevices = [NSArray arrayWithObjects:@"GAD_SIMULATOR_ID", nil];
    UIView *admobView = (UIView*)[self.view viewWithTag:990];
    [admobView addSubview:bannerView_];
    [bannerView_ loadRequest:request];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]init];
    [tap addTarget:self action:@selector(dissmissKeyboard:)];
    [inputView addGestureRecognizer:tap];
    
    [self.btnSearch setBackgroundColor:[UIColor colorWithRed:0.8 green:0 blue:0 alpha:1]];
    [self.btnTrack setBackgroundColor:[UIColor colorWithRed:0.95 green:0 blue:0 alpha:1]];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [self setBtnFind:nil];
    [self setTextFieldNumber:nil];
    [self setScrollView:nil];
    [self setTableViewFavorite:nil];
    [self setBtnSearch:nil];
    [self setBtnTrack:nil];
    [super viewDidUnload];
}

#pragma mark - UITableView Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  [dataTableArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    TrackListCell *cell = (TrackListCell*)[tableView dequeueReusableCellWithIdentifier:@"TrackListCell"];
    
    if(cell ==nil){
        cell = (TrackListCell*)[[[NSBundle mainBundle] loadNibNamed:@"TrackListCell" owner:nil options: nil] lastObject];
    }
    
    NSDictionary *dataDict = [dataTableArray objectAtIndex:[indexPath row]];
    cell.title.text = [dataDict objectForKey:@"name"];
    [cell.swither addTarget:self action:@selector(switchAlert:) forControlEvents:UIControlEventValueChanged];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if(editingStyle == UITableViewCellEditingStyleDelete){
        [dataTableArray removeObjectAtIndex:[indexPath row]];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationBottom];
    }
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    TrackListCell *cell = (TrackListCell*)[tableView cellForRowAtIndexPath:indexPath];
    if ([cell isEditing] == YES) {

    }
    else {
        NSDictionary *dataDict = [dataTableArray objectAtIndex:[indexPath row]];
        resultView = [[ResultViewController alloc] initWithNibName:@"ResultViewController" bundle:nil];
        resultView.emsID  = [dataDict objectForKey:@"ems"];
        [self.navigationController pushViewController:resultView animated:YES];
    }
}

-(void)editTable{
    
    [self.scrollView setContentOffset:CGPointMake(320, 0) animated:YES];
    
    if([self.tableViewFavorite isEditing]){
        self.navigationItem.leftBarButtonItem = editButton;
        [self.tableViewFavorite setEditing:NO animated:YES];
        
 
        [pref setObject:dataTableArray forKey:@"tracklist"];
        [pref synchronize];

    }else{
        self.navigationItem.leftBarButtonItem = donebutton;
        [self.tableViewFavorite setEditing:YES animated:YES];
    }
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    //NSLog(@"%f %f", scrollView.contentOffset.x, scrollView.contentOffset.y);
    self.title = @"ติดตามพัสดุ";

    [self.btnTrack setBackgroundColor:[UIColor colorWithRed:0.95 green:0 blue:0 alpha:1]];
    [self.btnSearch setBackgroundColor:[UIColor colorWithRed:0.8 green:0 blue:0 alpha:1]];
    
    if(scrollView.contentOffset.y != 0){
        return;
    }
    if (scrollView.contentOffset.x > 300) {
        self.title = @"Track List";
        [self.btnTrack setBackgroundColor:[UIColor colorWithRed:0.8 green:0 blue:0 alpha:1]];
        [self.btnSearch setBackgroundColor:[UIColor colorWithRed:0.95 green:0 blue:0 alpha:1]];
        self.navigationItem.leftBarButtonItem = editButton;
    }else if(scrollView.contentOffset.x < 300){
        self.navigationItem.leftBarButtonItem = nil;
    }
}

-(void)switchAlert:(id)sender{
    NSLog(@"TEST");
}

- (IBAction)btnFindAction:(id)sender {
    
    if([self.textFieldNumber.text isEqualToString:@""]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"พบข้อผิดพลาด" message:@"กรุณากรอกเลขที่ติดตามพัสดุด้วยคะ" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        return;
    }
    resultView = [[ResultViewController alloc] initWithNibName:@"ResultViewController" bundle:nil];
    resultView.emsID = self.textFieldNumber.text;
    NSLog(@"%@", self.textFieldNumber.text);
    [self.navigationController pushViewController:resultView animated:YES];
}
- (IBAction)btnShowTrackListAction:(id)sender {
    [self.scrollView setContentOffset:CGPointMake(320, 0) animated:YES];
}

- (IBAction)btnShowSearchAction:(id)sender {
    [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}

#pragma mark - TextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self btnFindAction:nil];
    return YES;
}

-(void)dissmissKeyboard:(id)sender{
    [self.textFieldNumber resignFirstResponder];
}
@end
