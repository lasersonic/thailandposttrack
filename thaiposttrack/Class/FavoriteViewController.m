//
//  FavoriteViewController.m
//  thaiposttrack
//
//  Created by Nutthawut on 6/1/13.
//  Copyright (c) 2013 Nut Tang. All rights reserved.
//

#import "FavoriteViewController.h"
#import "ResultViewController.h"

@interface FavoriteViewController (){
    NSMutableArray *favArray;
    NSUserDefaults *pref;
}

@end

@implementation FavoriteViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    pref = [NSUserDefaults standardUserDefaults];
    favArray = [NSMutableArray arrayWithArray:[pref objectForKey:@"tracklist"]];
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
//    [self.navigationItem.rightBarButtonItem setAction:@selector(editButtonAction:)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [favArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:@"FavoriteCell"];
    NSDictionary *dataDict = [favArray objectAtIndex:[indexPath row]];
    
    UILabel *title = (UILabel*)[cell viewWithTag:11];
    UILabel *datetime = (UILabel*)[cell viewWithTag:12];
    
    NSDate *d = [dataDict objectForKey:@"created"];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateStyle:NSDateFormatterShortStyle];
    title.text = [dataDict objectForKey:@"name"];
    datetime.text = [NSString stringWithFormat:@"%@",[formatter stringFromDate:d]];
    
    return cell;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if(editingStyle == UITableViewCellEditingStyleDelete){
        [favArray removeObjectAtIndex:[indexPath row]];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationBottom];
        
        [pref setObject:favArray forKey:@"tracklist"];
        [pref synchronize];
        
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ResultViewController *resultViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ResultViewController"];
    NSDictionary *dataDict = [favArray objectAtIndex:[indexPath row]];
    resultViewController.emsID = [dataDict objectForKey:@"ems"];
    [self.navigationController pushViewController:resultViewController animated:YES];
}

-(void)setEditing:(BOOL)editing animated:(BOOL)animated{
    [super setEditing:editing animated:animated];
    
    if(editing) {
        [UIView animateWithDuration:0.3 animations:^{
            self.tableView.editing = YES;
        }];
    }
    else {
        [UIView animateWithDuration:0.3 animations:^{
            self.tableView.editing = NO;
        }];
    }
}


- (void)viewDidUnload {
    [self setTableView:nil];
    [super viewDidUnload];
}
@end
