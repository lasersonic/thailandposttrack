//
//  ResultViewController.h
//  thaiposttrack
//
//  Created by Nutthawut on 5/4/13.
//  Copyright (c) 2013 Nut Tang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResultViewController : UIViewController<UIScrollViewDelegate, UIActionSheetDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;
@property (strong, nonatomic) IBOutlet UILabel *lblNotFound;
@property (strong, nonatomic) NSString* emsID;
- (IBAction)actionBookmark:(id)sender;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *barbuttonBookmark;
@end
