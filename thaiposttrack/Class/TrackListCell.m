//
//  TrackListCell.m
//  thaiposttrack
//
//  Created by Nutthawut on 5/5/13.
//  Copyright (c) 2013 Nut Tang. All rights reserved.
//

#import "TrackListCell.h"

@implementation TrackListCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
