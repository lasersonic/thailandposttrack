//
//  MainViewController.m
//  thaiposttrack
//
//  Created by Nutthawut on 6/1/13.
//  Copyright (c) 2013 Nut Tang. All rights reserved.
//

#import "MainViewController.h"
#import "SidebarViewController.h"
#import "ResultViewController.h"
#import "GADBannerView.h"
@interface MainViewController (){
    GADBannerView *bannerView_;
}

@end

@implementation MainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self addTabGesture];
    [self initAdmob];
}

-(void) addTabGesture{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]init];
    [tap addTarget:self action:@selector(dissmissKeyboard:)];
    [self.view addGestureRecognizer:tap];
}
-(void) initAdmob{
    //add Admob
    bannerView_ = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner];
    bannerView_.adUnitID = @"a151852abb5713f";
    bannerView_.rootViewController = self;
    GADRequest *request = [GADRequest request];
//    request.testDevices = [NSArray arrayWithObjects:@"GAD_SIMULATOR_ID", nil];
    UIView *admobView = (UIView*)[self.view viewWithTag:990];
    [admobView addSubview:bannerView_];
    [bannerView_ loadRequest:request];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dissmissKeyboard:(id)sender{
    [self.textFieldNumber resignFirstResponder];
}

- (IBAction)btnSearchAction:(id)sender {
    if([self.textFieldNumber.text isEqualToString:@""]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"พบข้อผิดพลาด" message:@"กรุณากรอกเลขที่ติดตามพัสดุด้วยคะ" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        return;
    }
    
    ResultViewController *resultView = [self.storyboard instantiateViewControllerWithIdentifier:@"ResultViewController"];
    resultView.emsID = self.textFieldNumber.text;
    NSLog(@"%@", self.textFieldNumber.text);
    [self.navigationController pushViewController:resultView animated:YES];
}
- (void)viewDidUnload {
    [self setTextFieldNumber:nil];
    [self setBtnSearch:nil];
    [super viewDidUnload];
}

#pragma mark - TextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self btnSearchAction:nil];
    return YES;
}
@end
