//
//  MainViewController.h
//  thaiposttrack
//
//  Created by Nutthawut on 6/1/13.
//  Copyright (c) 2013 Nut Tang. All rights reserved.
//

#import "RootViewController.h"

@interface MainViewController : RootViewController
- (IBAction)btnSearchAction:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *textFieldNumber;
@property (strong, nonatomic) IBOutlet UIButton *btnSearch;

@end
