//
//  ResultCell.h
//  thaiposttrack
//
//  Created by Nutthawut on 5/5/13.
//  Copyright (c) 2013 Nut Tang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResultCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *title;

@end
